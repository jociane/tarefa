package tarefa2Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Jociane Franzoni de Lima 1195409 Engenharia de Computação
 */
public class login extends HttpServlet {

    public login() {
        super();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String user = request.getParameter("login");
        String password = request.getParameter("senha");
        String profile = request.getParameter("perfil");

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        RequestDispatcher rd = null;
        request.setAttribute("login", user);
        request.setAttribute("perfil", profile);

       if (user != null && password != null && !user.equals("") && !password.equals("")) {
            if (user.equals(password)) {
                rd = request.getRequestDispatcher("/sucesso?" + user + "&" + profile);
                rd.forward(request, response);
            } else {
                //out.println("<b>Invalid Login Info.</b><br>");
                rd = request.getRequestDispatcher("erro.html");
                rd.include(request, response);
            }
        }

        out.close();
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
